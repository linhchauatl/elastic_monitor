elastic_monitor
===============

elasticsearch monitor. Send notification via HipChat and email when the indexes turn yellow and red.

![alt text](https://github.com/linhchauatl/animals/blob/master/public/images/snow_leopard.jpg "Snow Leopard")
