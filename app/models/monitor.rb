require File.expand_path('../../../config/initialize.rb', __FILE__)
require 'hipchat'

STATES = {
  ok:       :green,
  warning:  :yellow,
  critical: :red
}

class Monitor
  def initialize(server_name, server_url)
    @server_name = server_name
    @server_url  = server_url
  end

  def index_states
    indexes = `curl #{@server_url}:9200/_cat/indices`.split("\n")

    result = {}
    indexes.each do |index|
      data = index.split
      next unless ['red', 'green', 'yellow'].include?(data[0])
      index_state = result[data[0]] || []
      index_state << data[1]
      result[data[0]] = index_state
    end

    result
  end

  def warning(states)
    yellow_indexes = states['yellow']
    notify(:warning, yellow_indexes) #unless (yellow_indexes && yellow_indexes.empty?)
  end

  def critical(states)
    red_indexes = states['red']
    notify(:critical, red_indexes) #unless (red_indexes && red_indexes.empty?)
  end

  def notify(notification_type, indexes)
    prefix = "#{@server_name} elasticsearch: "
    client = HipChat::Client.new(HIPCHAT_TOKEN)

    if (indexes.nil? or indexes.empty?)
      # message = "#{prefix } There is nothing in #{notification_type}: #{STATES[notification_type]}."
      # puts message
      # client[HIPCHAT_ROOM_NAME].send('atweb', message, color: 'green')
      return
    end

    message = "\n\n#{Time.now}  #{prefix } The following indexes are in #{STATES[notification_type]}:\n#{indexes.join("\n")}"
    `echo "#{message}" >> ~/monitor.log`
    client[HIPCHAT_ROOM_NAME].send('atweb', "#{prefix } #{Time.now} There are #{indexes.size} indexes in #{notification_type}: #{STATES[notification_type]}.", color: STATES[notification_type])
    send_email(message)
  end

  def send_email(message)
    require 'net/smtp'
    current_host = 'localhost'
    opts = {}
    opts[:server]      ||= current_host
    opts[:from]        ||= EMAIL_FROM
    opts[:from_alias]  ||= 'Alert Emailer'
    opts[:subject]     ||= "#{@server_name} elasticsearch problem"
    opts[:body]        ||= message

    to = EMAIL_TO

    msg = "Subject: #{opts[:subject]}\n\n#{opts[:body]}"

    smtp = Net::SMTP.new SMTP_SERVER, SMTP_PORT
    smtp.enable_starttls
    smtp.start(current_host, EMAIL_FROM, EMAIL_FROM_PASSWORD, :login) do
      smtp.send_message(msg, opts[:from], to)
    end
  end
end


monitors = []

SERVERS.each do |server|
  server_name, server_url = server.split('@')
  monitors << Monitor.new(server_name, server_url)
end

while true do
  monitors.each do |monitor|
    states = monitor.index_states

    #puts("\n**********************************************\n")
    monitor.warning(states)

    #puts("\n**********************************************\n")
    monitor.critical(states)
  end
  sleep(180)
  `rm ~/monitor.log`
end