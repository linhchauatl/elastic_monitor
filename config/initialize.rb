require 'yaml'
CONFIG = YAML.load(File.read(File.expand_path('../settings.yml', __FILE__)))

SERVERS           = CONFIG['servers']
MONITOR_SERVER    = CONFIG['monitor_server']

HIPCHAT_TOKEN     = CONFIG['hipchat_token']
HIPCHAT_ROOM_NAME = CONFIG['hipchat_room_name']

EMAIL_FROM          = CONFIG['email_from']
EMAIL_TO            = CONFIG['email_to']
EMAIL_FROM_PASSWORD = CONFIG['email_from_password']

SMTP_SERVER = CONFIG['smtp_server']
SMTP_PORT   = CONFIG['smtp_port']